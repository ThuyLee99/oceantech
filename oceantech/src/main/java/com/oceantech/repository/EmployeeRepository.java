package com.oceantech.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.oceantech.model.Employee;

@Repository
public interface  EmployeeRepository extends JpaRepository<Employee, Integer>{
    @Query("select e from Employee e " +
            "where e.name like concat('%', ?1, '%')" +
            "or e.code like concat('%', ?1, '%')")
    List<Employee> findByNameOrCode(String search);
}
