package com.oceantech.validate;

public class Length
{
    private static final String MESSAGE = "%sは%s文字以内で入力して下さい。 ";

    private static final String INVALID_LENGTH_MESSAGE = "%sは%s文字以上%s文字以下で入力して下さい。";

    private static final String INVALID_FIXED_LENGTH_MESSAGE = "%sは%s文字で入力して下さい。";

    public static String valid( int max, String val, String columnName )
    {
        if ( isEmptyVal( val ) )
            return "";

        if ( val.length() > max )
        {
            return String.format( MESSAGE, columnName, max );
        }
        return "";
    }

    public static String valid( int min, int max, String val, String columnName )
    {
        if ( isEmptyVal( val ) )
            return "";

        if ( val.length() < min || val.length() > max )
        {
            if ( min == max )
            {
                return String.format( INVALID_FIXED_LENGTH_MESSAGE, columnName, min );
            }
            else
            {
                return String.format( INVALID_LENGTH_MESSAGE, columnName, min, max );
            }
        }
        return "";
    }

    public static boolean invalid( String val, int max )
    {
        String message = valid( max, val, "" );
        if(message.isEmpty())
            return false;
        return true;
    }

    public static boolean invalid( String val, int min, int max )
    {
        String message = valid( min, max, val, "" );
        if(message.isEmpty())
            return false;
        return true;
    }

    public static String errorMessage( String label, int max )
    {
        return String.format( MESSAGE, label, max );
    }

    public static String errorMessage( String label, int min, int max )
    {
        if ( min == max )
            return String.format( INVALID_FIXED_LENGTH_MESSAGE, label, min );
        return String.format( INVALID_LENGTH_MESSAGE, label, min, max );
    }

    public static boolean isEmptyVal( String val )
    {
        return val == null || val.isEmpty();
    }
}
