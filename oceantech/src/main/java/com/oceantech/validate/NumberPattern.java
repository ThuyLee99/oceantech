package com.oceantech.validate;

public class NumberPattern
{
    public static final String REGEX = "^-?[0-9]*\\.?[0-9]+$";

    private static final String PATTERN_INVALID_FORMAT = "%sPlease enter correctly。";

    public static String valid( String val, String column )
    {
        StringBuilder errorMessage = new StringBuilder();

        if( isInvalid( val, column ) )
        {
            errorMessage.append( String.format( PATTERN_INVALID_FORMAT, column ) );
        }

        return errorMessage.toString();
    }

    public static boolean isInvalid( String val, String column )
    {
        if( isEmptyVal( val ) )
            return false;

        if( val.indexOf( "-" ) == 0 ) // TODO:要リファクタリング。正規表現が不十分なのをif文で回避している。
            return true;
        if( val.indexOf( "." ) == 0 )
            return true;
        if( !val.matches( REGEX ) )
            return true;

        return false;
    }

    public static String errorMessage( String label )
    {
        return String.format( PATTERN_INVALID_FORMAT, label );
    }

    private static boolean isEmptyVal( String val )
    {
        return val == null || val.isEmpty();
    }
}
