package com.oceantech.validate;

public class Required
{
    private static final String REQUIRED_INVALID_FORMAT = "%srequired";

    public static String valid( String val, String column )
    {
        StringBuilder errorMessage = new StringBuilder();

        if( isEmptyVal( val ) )
        {
            errorMessage.append( String.format( REQUIRED_INVALID_FORMAT, column ) );
        }

        return errorMessage.toString();
    }

    public static String valid( Object val, String column )
    {
        StringBuilder errorMessage = new StringBuilder();

        if( isEmptyVal( val ) )
        {
            errorMessage.append( String.format( REQUIRED_INVALID_FORMAT, column ) );
        }

        return errorMessage.toString();
    }

    public static String errorMessage( String column )
    {
        return String.format( REQUIRED_INVALID_FORMAT, column );
    }

    public static boolean isEmptyVal( String val )
    {
        return val == null || val.isEmpty();
    }

    public static boolean isEmptyVal( Object val )
    {
        return val == null;
    }
}
