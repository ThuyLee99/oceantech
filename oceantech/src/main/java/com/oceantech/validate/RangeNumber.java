package com.oceantech.validate;

public class RangeNumber
{
    private static final String INVALID_MORE_NUMBER = "%sPlease enter the following numerical values.";

    private static final String INVALID_LESS_NUMBER = "%sPlease enter the above numbers.";

    private static final String INVALID_OUTSIDE_NUMBER = "%sPlease%s～% enter in the range";

    public static String valid( int max, int min, String val, String columnName )
    {
        if ( isInvalidNumber( val ) )
            return "";

        Long verification = 0L;
        try
        {
            verification = Long.parseLong( val );
        }
        catch ( NumberFormatException e )
        {
            return String.format( INVALID_MORE_NUMBER, columnName, max );
        }

        if ( verification < min || verification > max )
            return String.format( INVALID_OUTSIDE_NUMBER, columnName, min, max );
        else
            return "";
    }

    public static String valid( int max, String val, String columnName )
    {
        if ( isInvalidNumber( val ) )
            return "";

        Long verification = 0L;
        try
        {
            verification = Long.parseLong( val );
        }
        catch ( NumberFormatException e )
        {
            return String.format( INVALID_MORE_NUMBER, columnName, max );
        }

        if ( verification > max )
            return String.format( INVALID_MORE_NUMBER, columnName, max );
        else
            return "";
    }

    public static boolean invalid( int min, int max, String val )
    {
        String message = valid( max, min, val, "" );
        boolean invalid = !message.isEmpty();
        return invalid;
    }

    public static boolean invalidMore( int max, String val )
    {
        String message = valid( max, val, "" );
        boolean invalid = !message.isEmpty();
        return invalid;
    }

    public static boolean invalidLess( int min, String val )
    {
        if ( isInvalidNumber( val ) )
            return false;

        Long verification = 0L;
        try
        {
            verification = Long.parseLong( val );
        }
        catch ( NumberFormatException e )
        {
            return true;
        }

        return verification < min;
    }

    public static String errorMessage( String label, int min, int max )
    {
        return String.format( INVALID_OUTSIDE_NUMBER, label, min, max );
    }

    public static String errorMessageMore( String label, int max )
    {
        return String.format( INVALID_MORE_NUMBER, label, max );
    }

    public static String errorMessageLess( String label, int min )
    {
        return String.format( INVALID_LESS_NUMBER, label, min );
    }

    private static boolean isInvalidNumber( String val )
    {
        if ( !Required.valid( val, "" ).isEmpty() )
            return true;
        else if ( !NumberPattern.valid( val, "" ).isEmpty() )
            return true;
        else
            return false;
    }
}
