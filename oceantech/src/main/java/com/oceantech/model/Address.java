package com.oceantech.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "address" )
public class Address
{
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "id" )
    private Integer id;

    @Column( name = "province_id" )
    private Integer provinceId;

    @Column( name = "province" )
    private String province;

    @Column( name = "district_id" )
    private Integer districtId;

    @Column( name = "district" )
    private String district;

    @Column( name = "commune_id" )
    private Integer communeId;

    @Column( name = "commune" )
    private Integer commune;

    public Integer getId()
    {
        return id;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }

    public Integer getProvinceId()
    {
        return provinceId;
    }

    public void setProvinceId( Integer provinceId )
    {
        this.provinceId = provinceId;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince( String province )
    {
        this.province = province;
    }

    public Integer getDistrictId()
    {
        return districtId;
    }

    public void setDistrictId( Integer districtId )
    {
        this.districtId = districtId;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict( String district )
    {
        this.district = district;
    }

    public Integer getCommuneId()
    {
        return communeId;
    }

    public void setCommuneId( Integer communeId )
    {
        this.communeId = communeId;
    }

    public Integer getCommune()
    {
        return commune;
    }

    public void setCommune( Integer commune )
    {
        this.commune = commune;
    }

    public Address( Integer id, Integer provinceId, String province, Integer districtId, String district, Integer communeId, Integer commune )
    {
        super();
        this.id = id;
        this.provinceId = provinceId;
        this.province = province;
        this.districtId = districtId;
        this.district = district;
        this.communeId = communeId;
        this.commune = commune;
    }

    public Address()
    {
        super();
    }

}
