package com.oceantech.service.Impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.oceantech.DTO.EmployeeDTO;
import com.oceantech.common.ModelMapperUtils;
import com.oceantech.model.Employee;
import com.oceantech.repository.EmployeeRepository;
import com.oceantech.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService
{
    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl( EmployeeRepository employeeRepository )
    {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<EmployeeDTO> search(String search)
    {
        search = search.replaceAll("\\W", "\\\\$0");
        List<Employee> employees = employeeRepository.findByNameOrCode( search );
        List<EmployeeDTO> employeeDTOs = employees.stream().map( employee -> ModelMapperUtils.map( employee, EmployeeDTO.class ) )
                .collect( Collectors.toList() );
        return employeeDTOs;
    }

    @Override
    @Transactional( rollbackFor = Exception.class )
    public EmployeeDTO createEmployee( EmployeeDTO employeeDTO )
    {
        Employee employee = ModelMapperUtils.map( employeeDTO, Employee.class );
        Employee save = employeeRepository.save( employee );
        return ModelMapperUtils.map( save, EmployeeDTO.class );
    }

    @Override
    public EmployeeDTO editEmployee( Integer id, EmployeeDTO employeeDTO )
    {
        Optional<Employee> employee = employeeRepository.findById( id );
        if ( !employee.isPresent() )
        {
            throw new ResponseStatusException( HttpStatus.BAD_REQUEST, "employee does not exits" );
        }
        employee.get().setCode( employeeDTO.getCode() );
        employee.get().setEmail( employeeDTO.getEmail() );
        employee.get().setName( employeeDTO.getName() );
        employee.get().setPhone( employeeDTO.getPhone() );
        employee.get().setAge( employeeDTO.getAge() );
        Employee save = employeeRepository.save( employee.get() );
        return ModelMapperUtils.map( save, EmployeeDTO.class );
    }

    @Override
    public Boolean delete( Integer id )
    {
        Optional<Employee> employee = employeeRepository.findById( id );
        if ( !employee.isPresent() )
        {
            throw new ResponseStatusException( HttpStatus.BAD_REQUEST, "employee does not exits" );
        }
        employeeRepository.delete( employee.get() );
        return true;
    }

    @Override
    public List<EmployeeDTO> findAll()
    {
        List<Employee> employees = employeeRepository.findAll();
        List<EmployeeDTO> employeeDTOs = employees.stream().map( employee -> ModelMapperUtils.map( employee, EmployeeDTO.class ) )
                .collect( Collectors.toList() );
        return employeeDTOs;
    }

}
