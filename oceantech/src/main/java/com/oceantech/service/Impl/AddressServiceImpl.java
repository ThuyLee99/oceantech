package com.oceantech.service.Impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;

import com.oceantech.DTO.AddressDTO;
import com.oceantech.common.ModelMapperUtils;
import com.oceantech.model.Address;
import com.oceantech.repository.AddressRepository;
import com.oceantech.service.AddressService;

public class AddressServiceImpl implements AddressService
{
    private final AddressRepository addressRepository;

    public AddressServiceImpl( AddressRepository addressRepository )
    {
        this.addressRepository = addressRepository  ;
    }
    @Override
    public List<AddressDTO> findAll()
    {
        List<Address> addresses = addressRepository.findAll();
        List<AddressDTO> addressDTOs = addresses.stream().map( addresse -> ModelMapperUtils.map( addresse, AddressDTO.class ) )
                .collect( Collectors.toList() );
        return addressDTOs;
    }

    @Override
    @Transactional( rollbackFor = Exception.class )
    public AddressDTO createEmployee( AddressDTO addressDTO )
    {
        Address address = ModelMapperUtils.map( addressDTO, Address.class );
        Address save = addressRepository.save( address );
        return ModelMapperUtils.map( save, AddressDTO.class );
    }

    @Override
    public AddressDTO editAddress( Integer id, AddressDTO addressDTO )
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean delete( Integer id )
    {
        // TODO Auto-generated method stub
        return null;
    }

}
