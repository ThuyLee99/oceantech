package com.oceantech.service;

import java.util.List;

import com.oceantech.DTO.AddressDTO;

public interface AddressService
{
    List<AddressDTO> findAll();

    AddressDTO createEmployee( AddressDTO addressDTO );

    AddressDTO editAddress( Integer id, AddressDTO addressDTO );

    Boolean delete( Integer id );
}
