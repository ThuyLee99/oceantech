package com.oceantech.service;

import java.util.List;

import com.oceantech.DTO.EmployeeDTO;

public interface EmployeeService
{
    List<EmployeeDTO> findAll();
    
    List<EmployeeDTO> search(String search);

    EmployeeDTO createEmployee( EmployeeDTO employeeDTO );

    EmployeeDTO editEmployee( Integer id, EmployeeDTO employeeDTO );

    Boolean delete( Integer id );
}
