package com.oceantech.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oceantech.DTO.EmployeeDTO;
import com.oceantech.common.UserExcelExporter;
import com.oceantech.service.EmployeeService;

@RestController
@RequestMapping( "/api/employee" )
public class EmloyeeController
{
    private final EmployeeService employeeService;

    public EmloyeeController( EmployeeService employeeService )
    {
        this.employeeService = employeeService;
    }

//    @GetMapping( "" )
//    private ResponseEntity<List<EmployeeDTO>> findAll()
//    {
//        return ResponseEntity.ok( employeeService.findAll() );
//    }

    @PostMapping( "" )
    private ResponseEntity<EmployeeDTO> create( @RequestBody EmployeeDTO employeeDTO )
    {
        return ResponseEntity.ok( employeeService.createEmployee( employeeDTO ) );
    }

    @PutMapping( "/{id}" )
    private ResponseEntity<EmployeeDTO> edit( @PathVariable Integer id, @RequestBody EmployeeDTO employeeDTO )
    {
        return ResponseEntity.ok( employeeService.editEmployee( id, employeeDTO ) );
    }

    @DeleteMapping( "/{id}" )
    private ResponseEntity<Boolean> delete( @PathVariable Integer id ) throws Exception
    {
        return ResponseEntity.ok( employeeService.delete( id ) );
    }

    @GetMapping( "/export/excel" )
    private void export(HttpServletResponse response) throws IOException
    {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
         
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=employee_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
         
        List<EmployeeDTO> employees = employeeService.findAll();
         
        UserExcelExporter excelExporter = new UserExcelExporter(employees);
         
        excelExporter.export(response); 
    }
    @GetMapping( "" )
    private ResponseEntity<List<EmployeeDTO>> search(@RequestParam(name = "search") String search)
    {
        return ResponseEntity.ok( employeeService.search( search ) );
    }
}
