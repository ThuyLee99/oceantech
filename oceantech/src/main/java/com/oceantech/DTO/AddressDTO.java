package com.oceantech.DTO;

public class AddressDTO
{
    private Integer provinceId;

    private String province;

    private Integer districtId;

    private String district;

    private Integer communeId;

    private Integer commune;

    public Integer getProvinceId()
    {
        return provinceId;
    }

    public void setProvinceId( Integer provinceId )
    {
        this.provinceId = provinceId;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince( String province )
    {
        this.province = province;
    }

    public Integer getDistrictId()
    {
        return districtId;
    }

    public void setDistrictId( Integer districtId )
    {
        this.districtId = districtId;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict( String district )
    {
        this.district = district;
    }

    public Integer getCommuneId()
    {
        return communeId;
    }

    public void setCommuneId( Integer communeId )
    {
        this.communeId = communeId;
    }

    public Integer getCommune()
    {
        return commune;
    }

    public void setCommune( Integer commune )
    {
        this.commune = commune;
    }

    public AddressDTO( Integer provinceId, String province, Integer districtId, String district, Integer communeId, Integer commune )
    {
        super();
        this.provinceId = provinceId;
        this.province = province;
        this.districtId = districtId;
        this.district = district;
        this.communeId = communeId;
        this.commune = commune;
    }

    public AddressDTO()
    {
        super();
    }

}
