package com.oceantech.DTO;

public class EmployeeSearchDTO {
    private String code;
    
    private String name;
    
    private String email;
    
    private String phone;
    
    private Integer age;
}
